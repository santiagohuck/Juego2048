/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import java.awt.*;
import java.awt.event.*;
import java.util.Properties;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.*;

public class TableDemo extends JFrame {

    public TableDemo() {
        super("Rollover-Table-Demo");

        // setup menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic('F');
        JMenuItem menuItem = new JMenuItem("Exit");
        menuItem.setMnemonic('x');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_MASK));
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        // setup widgets
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4));

        JTable myTable = new MyTable(new MyTableModel());
        myTable.setRowHeight(24);
        myTable.setSelectionBackground(Color.orange);
        myTable.setSelectionForeground(Color.black);
        myTable.getSelectionModel().setSelectionInterval(2, 2);
        TableColumn tableCol = myTable.getColumnModel().getColumn(2);
        JComboBox comboBox = new JComboBox();
        comboBox.addItem("Snowboarding");
        comboBox.addItem("Rowing");
        comboBox.addItem("Skating");
        comboBox.addItem("Chasing toddlers");
        comboBox.addItem("Speed reading");
        comboBox.addItem("Teaching high school");
        comboBox.addItem("None");
        tableCol.setCellEditor(new DefaultCellEditor(comboBox));
        tableCol.setPreferredWidth(200);

        tableCol = myTable.getColumnModel().getColumn(1);
        tableCol.setPreferredWidth(80);

        contentPanel.add(new JScrollPane(myTable), BorderLayout.CENTER);
        setContentPane(contentPanel);

        // add listeners
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // show application
        setLocation(32, 32);
        setSize(480, 300);
        setVisible(true);
    } // end CTor TableDemo

//---------------------------------------------------------------------------------------
    public class MyTable extends JTable {
        private Color evenBackColor = new Color(80, 80, 80);
        private Color evenTextColor = Color.white;
        private Color oddBackColor = new Color(96, 96, 96);
        private Color oddTextColor = Color.white;
        private Color rolloverBackColor = new Color(128, 128, 128);
        private Color rolloverTextColor = Color.white;

        private int rolloverRowIndex = -1;

        public MyTable(TableModel model) {
            super(model);
            RolloverListener listener = new RolloverListener();
            addMouseMotionListener(listener);
            addMouseListener(listener);
        }

        public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            Component c = super.prepareRenderer(renderer, row, column);
            if (isRowSelected(row)) {
                c.setForeground(getSelectionForeground());
                c.setBackground(getSelectionBackground());
            }
            else if (row == rolloverRowIndex) {
                c.setForeground(rolloverTextColor);
                c.setBackground(rolloverBackColor);
            }
            else if (row % 2 == 0) {
                c.setForeground(evenTextColor);
                c.setBackground(evenBackColor);
            }
            else {
                c.setForeground(oddTextColor);
                c.setBackground(oddBackColor);
            }
            return c;
        }

        private class RolloverListener extends MouseInputAdapter {

            public void mouseExited(MouseEvent e) {
                rolloverRowIndex = -1;
                repaint();
            }

            public void mouseMoved(MouseEvent e) {
                int row = rowAtPoint(e.getPoint());
                if (row != rolloverRowIndex) {
                    rolloverRowIndex = row;
                    repaint();
                }
            }
        }
    }

//---------------------------------------------------------------------------------------
    class MyTableModel extends AbstractTableModel {

        private String[] columnNames = {"First Name", "Last Name", "Sport", "# of Years", "Vegetarian"};
        private Object[][] data = {{"Mary", "Campione", "Snowboarding", new Integer(5), Boolean.FALSE},
                                    {"Alison", "Huml", "Rowing", new Integer(3), Boolean.TRUE},
                                    {"Kathy", "Walrath", "Knitting", new Integer(2), Boolean.FALSE},
                                    {"Sharon", "Zakhour", "Speed reading", new Integer(20), Boolean.TRUE},
                                    {"Mimi", "Koslowski", "Skating", new Integer(7), Boolean.TRUE},
                                    {"August", "Vanderbilt", "Ice hopping", new Integer(2),Boolean.FALSE},
                                    {"Philip", "Milne", "Pool", new Integer(10), Boolean.FALSE}};

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell. If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's editable.
         */
        public boolean isCellEditable(int row, int col) {
            return true;
        }

        /*
         * Don't need to implement this method unless your table's data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }

//---------------------------------------------------------------------------------------
    public static void main(String[] args) {
//---------------------------------------------------------------------------------------
        try {
            // select Look and Feel
            Properties props = new Properties();
            props.put("logoString", "my company");
            props.put("textShadow", "off");
            props.put("systemTextFont", "Arial PLAIN 13");
            props.put("controlTextFont", "Arial PLAIN 13");
            props.put("menuTextFont", "Arial PLAIN 13");
            props.put("userTextFont", "Arial PLAIN 13");
            props.put("subTextFont", "Arial PLAIN 12");
            props.put("windowTitleFont", "Arial BOLD 13");
            com.jtattoo.plaf.hifi.HiFiLookAndFeel.setCurrentTheme(props);
            UIManager.setLookAndFeel("com.jtattoo.plaf.hifi.HiFiLookAndFeel");
            // start application
            new TableDemo();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } // end main
} // end class TableDemo