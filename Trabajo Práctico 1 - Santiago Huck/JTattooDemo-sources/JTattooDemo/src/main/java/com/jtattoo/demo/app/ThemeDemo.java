/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import java.awt.*;
import java.awt.event.*;
import java.util.Properties;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.table.*;

public class ThemeDemo extends JFrame {

    public static ThemeDemo app = null;
    public JPopupMenu popup = null;

    public ThemeDemo() {
        super("Theme-Demo-App");

        // setup menu
        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        menu.setMnemonic('F');
        JMenuItem menuItem = new JMenuItem("New");
        menu.add(menuItem);
        menuItem = new JMenuItem("Open");
        menuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                JFileChooser fc = new JFileChooser();
                int result = fc.showOpenDialog(app);
            }
        });

        menu.add(menuItem);
        menuItem = new JMenuItem("Save");
        menu.add(menuItem);
        menuItem = new JMenuItem("Save as");
        menu.add(menuItem);
        menu.addSeparator();
        menuItem = new JMenuItem("Exit");
        menuItem.setMnemonic('x');
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, KeyEvent.ALT_MASK));
        menuItem.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        // setup the widgets
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBorder(BorderFactory.createEmptyBorder(0, 4, 4, 4));
        JTree tree = new JTree();
        tree.expandRow(3);
        tree.expandRow(2);
        tree.expandRow(1);
        JScrollPane westPanel = new JScrollPane(tree);

        JScrollPane formScrollPane = new JScrollPane(createFormPanel());
        formScrollPane.setMinimumSize(new Dimension(-1, 80));
        final JTable table = new MyTable(new MyTableModel());
        final JScrollPane tableScrollPane = new JScrollPane(table);
        JSplitPane horSplitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, true, formScrollPane, tableScrollPane);
        horSplitPane.setDividerLocation(120);
        JSplitPane verSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true, westPanel, horSplitPane);
        verSplitPane.setDividerLocation(148);
        contentPanel.add(verSplitPane, BorderLayout.CENTER);
        setContentPane(contentPanel);

        // add the listeners
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // show the application
        setLocation(32, 32);
        setSize(600, 400);
        setVisible(true);
    } // end CTor ThemeDemoApp

    private JPanel createFormPanel() {
        JPanel panel = new JPanel(null);
        panel.setPreferredSize(new Dimension(425, 100));

        JLabel firstnameLabel = new JLabel("Firstname");
        firstnameLabel.setBounds(10, 10, 100, 20);
        panel.add(firstnameLabel);

        JTextField firstnameField = new JTextField();
        firstnameField.setBounds(110, 10, 240, 20);
        firstnameField.setText("Donald");
        panel.add(firstnameField);

        JLabel lastnameLabel = new JLabel("Lastname");
        lastnameLabel.setBounds(10, 35, 100, 20);
        lastnameLabel.setEnabled(false);
        panel.add(lastnameLabel);

        JTextField lastnameField = new JTextField();
        lastnameField.setBounds(110, 35, 240, 20);
        lastnameField.setText("Duck");
        lastnameField.setEnabled(false);
        panel.add(lastnameField);

        JButton okButton = new JButton("OK");
        okButton.setBounds(360, 10, 60, 22);
        panel.add(okButton);

        return panel;
    }

//---------------------------------------------------------------------------------------
    public class MyTable extends JTable {

        private int rolloverRowIndex = -1;

        public MyTable(TableModel model) {
            super(model);
            RolloverListener listener = new RolloverListener();
            addMouseMotionListener(listener);
            addMouseListener(listener);
        }

        public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
            Component c = super.prepareRenderer(renderer, row, column);
            Color foreground = getForeground();
            Color background = getBackground();
            if (isRowSelected(row)) {
                foreground = getSelectionForeground();
                background = getSelectionBackground();
            } else if (row == rolloverRowIndex) {
                background = com.jtattoo.plaf.ColorHelper.brighter(background, 30);
            } else if (row % 2 == 0) {
                background = com.jtattoo.plaf.ColorHelper.brighter(background, 10);
            }
            c.setForeground(foreground);
            c.setBackground(background);
            return c;
        }

        private class RolloverListener extends MouseInputAdapter {

            public void mouseExited(MouseEvent e) {
                rolloverRowIndex = -1;
                repaint();
            }

            public void mouseMoved(MouseEvent e) {
                int row = rowAtPoint(e.getPoint());
                if (row != rolloverRowIndex) {
                    rolloverRowIndex = row;
                    repaint();
                }
            }
        }
    }

//---------------------------------------------------------------------------------------
    class MyTableModel extends AbstractTableModel {

        private String[] columnNames = {"Firstname", "Lastname", "Sport", "# of Years", "Vegetarian"};
        private Object[][] data = {
            {"Mary", "Campione", "Snowboarding", new Integer(5), Boolean.FALSE},
            {"Alison", "Huml", "Rowing", new Integer(3), Boolean.TRUE},
            {"Kathy", "Walrath", "Knitting", new Integer(2), Boolean.FALSE},
            {"Sharon", "Zakhour", "Speed reading", new Integer(20), Boolean.TRUE},
            {"Philip", "Milne", "Pool", new Integer(10), Boolean.FALSE}
        };

        public int getColumnCount() {
            return columnNames.length;
        }

        public int getRowCount() {
            return data.length;
        }

        public String getColumnName(int col) {
            return columnNames[col];
        }

        public Object getValueAt(int row, int col) {
            return data[row][col];
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell. If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        public Class getColumnClass(int c) {
            return getValueAt(0, c).getClass();
        }

        /*
         * Don't need to implement this method unless your table's editable.
         */
        public boolean isCellEditable(int row, int col) {
            return true;
        }

        /*
         * Don't need to implement this method unless your table's data can change.
         */
        public void setValueAt(Object value, int row, int col) {
            data[row][col] = value;
            fireTableCellUpdated(row, col);
        }
    }

//---------------------------------------------------------------------------------------
    public static void main(String[] args) {
//---------------------------------------------------------------------------------------
        try {
            // setup the look and feel properties
            Properties props = new Properties();
            props.put("logoString", "my company");
//            props.put("logoString", "\u00A0");
            props.put("backgroundPattern", "off");

            props.put("windowTitleForegroundColor", "228 228 255");
            props.put("windowTitleBackgroundColor", "0 0 96");
            props.put("windowTitleColorLight", "0 0 96");
            props.put("windowTitleColorDark", "0 0 64");
            props.put("windowBorderColor", "96 96 64");

            props.put("windowInactiveTitleForegroundColor", "228 228 255");
            props.put("windowInactiveTitleBackgroundColor", "0 0 96");
            props.put("windowInactiveTitleColorLight", "0 0 96");
            props.put("windowInactiveTitleColorDark", "0 0 64");
            props.put("windowInactiveBorderColor", "32 32 128");

            props.put("menuForegroundColor", "228 228 255");
            props.put("menuBackgroundColor", "0 0 64");
            props.put("menuSelectionForegroundColor", "0 0 0");
            props.put("menuSelectionBackgroundColor", "255 192 48");
            props.put("menuColorLight", "32 32 128");
            props.put("menuColorDark", "16 16 96");

            props.put("toolbarColorLight", "32 32 128");
            props.put("toolbarColorDark", "16 16 96");

            props.put("controlForegroundColor", "228 228 255");
            props.put("controlBackgroundColor", "16 16 96");
            props.put("controlColorLight", "16 16 96");
            props.put("controlColorDark", "8 8 64");
            props.put("controlHighlightColor", "32 32 128");
            props.put("controlShadowColor", "16 16 64");
            props.put("controlDarkShadowColor", "8 8 32");

            props.put("buttonForegroundColor", "0 0 32");
            props.put("buttonBackgroundColor", "196 196 196");
            props.put("buttonColorLight", "196 196 240");
            props.put("buttonColorDark", "164 164 228");

            props.put("foregroundColor", "228 228 255");
            props.put("backgroundColor", "0 0 64");
            props.put("backgroundColorLight", "16 16 96");
            props.put("backgroundColorDark", "8 8 64");
            props.put("alterBackgroundColor", "255 0 0");
            props.put("frameColor", "96 96 64");
            props.put("gridColor", "96 96 64");
            props.put("focusCellColor", "240 0 0");

            props.put("disabledForegroundColor", "128 128 164");
            props.put("disabledBackgroundColor", "0 0 72");

            props.put("selectionForegroundColor", "0 0 0");
            props.put("selectionBackgroundColor", "196 148 16");

            props.put("inputForegroundColor", "228 228 255");
            props.put("inputBackgroundColor", "0 0 96");

            props.put("rolloverColor", "240 168 0");
            props.put("rolloverColorLight", "240 168 0");
            props.put("rolloverColorDark", "196 137 0");

            // set your theme
            com.jtattoo.plaf.noire.NoireLookAndFeel.setCurrentTheme(props);
            // select the Look and Feel
            UIManager.setLookAndFeel("com.jtattoo.plaf.noire.NoireLookAndFeel");

            app = new ThemeDemo();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } // end main

}
