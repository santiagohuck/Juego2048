/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TabDemo extends JFrame {
    
    private static final int MAX_TABS = 10;
    
    private JTabbedPane tabPane = null;
    
    public TabDemo() {
        super("TabPane-Demo");
        
        // setup widgets
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));

        // init some tabs
        tabPane = new JTabbedPane();
        tabPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        for (int i = 0; i < 3; i++) {
            tabPane.add("", new JPanel());
            tabPane.setTabComponentAt(i, new CloseableTabComponent(tabPane));
        }
        // adding the last tab with functionallity to add new tabs
        tabPane.addTab("", new JPanel());
        tabPane.setTabComponentAt(tabPane.getTabCount() - 1, new LastTabComponent(tabPane));
        // this tab must not be enabled because we don't want to select this tab
        tabPane.setEnabledAt(tabPane.getTabCount() - 1, false);
        
        contentPanel.add(tabPane, BorderLayout.CENTER);
        
        setContentPane(contentPanel);
        
        // add listeners
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        
        // show application
        setSize(800, 400);
        setLocationRelativeTo(null);
        setVisible(true);
    } 
    
    private static class CloseableTabComponent extends JPanel {
        
        private static int tabNo = 0;
        private JLabel titleLabel = null;
        private JButton closeButton = null; 
        private JTabbedPane tabPane = null;
        
        public CloseableTabComponent(JTabbedPane aTabbedPane) {
            super(new BorderLayout());
            tabPane = aTabbedPane;
            
            setOpaque(false);
            setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 0));
            
            titleLabel = new JLabel("Tab " + (++tabNo) + " ");
            titleLabel.setOpaque(false);

            closeButton = new JButton("X");
            closeButton.setFont(new Font("Dialog", Font.BOLD, 14));
            closeButton.setForeground(Color.red);
            closeButton.setBorderPainted(false);
            
            closeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    for (int i = 0; i < tabPane.getTabCount(); i++) {
                        if (TabDemo.CloseableTabComponent.this.equals(tabPane.getTabComponentAt(i))) {
                            tabPane.removeTabAt(i);
                            break;
                        }
                    }
                    if ((tabPane.getTabCount() > 1) && (tabPane.getSelectedIndex() == tabPane.getTabCount() - 1)) {
                        tabPane.setSelectedIndex(tabPane.getTabCount() - 2);
                    }
                }
            });
            
            add(titleLabel, BorderLayout.CENTER);
            add(closeButton, BorderLayout.EAST);
        }
    }
    
    private static class LastTabComponent extends JPanel {
        
        private JButton addButton = null; 
        private JTabbedPane tabPane = null;
        
        public LastTabComponent(JTabbedPane aTabbedPane) {
            super(new BorderLayout());
            tabPane = aTabbedPane;
            
            setOpaque(false);
            setBorder(BorderFactory.createEmptyBorder(1, 0, 0, 0));
            
            addButton = new JButton("+");
            addButton.setFont(new Font("Dialog", Font.BOLD, 14));
            addButton.setForeground(Color.blue);
            addButton.setBorderPainted(false);
            
            addButton.addActionListener(new ActionListener() {
                
                public void actionPerformed(ActionEvent e) {
                    if (tabPane.getTabCount() < MAX_TABS) {
                        int tabIndex = tabPane.getTabCount() - 1;
                        tabPane.insertTab("Tab", null, new JPanel(), null, tabIndex);
                        tabPane.setTabComponentAt(tabIndex, new CloseableTabComponent(tabPane));
                        SwingUtilities.invokeLater(new Runnable() {

                            public void run() {
                                if (tabPane.getTabCount() > 1) {
                                    tabPane.setSelectedIndex(tabPane.getTabCount() - 2);
                                }
                            }
                        });
                    }
                }
            });
            add(addButton, BorderLayout.EAST);
        }
    }
    
    public static void main(String[] args) {
        try {
            // select Look and Feel
            UIManager.setLookAndFeel("com.jtattoo.plaf.smart.SmartLookAndFeel");
            // start application
            new TabDemo();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    } // end main
    
}     