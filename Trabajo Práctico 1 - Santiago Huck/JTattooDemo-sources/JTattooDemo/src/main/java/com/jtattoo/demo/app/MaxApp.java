/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import com.jtattoo.plaf.aero.AeroLookAndFeel;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * @author Adam Dyga, Michael Hagen
 *
 */
public class MaxApp extends JFrame {

    public MaxApp() {
        super("Maximize-Test");
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        panel.setBorder(new TitledBorder("Test"));

        getContentPane().add(panel);

        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("First", new JPanel());
        tabPane.addTab("Second", new JPanel());
        tabPane.addTab("Third", new JPanel());
        tabPane.setEnabledAt(1, false);
        panel.add(tabPane, BorderLayout.CENTER);
        setSize(new Dimension(800, 600));
        setLocationRelativeTo(null);
        calculateBounds();

        addPropertyChangeListener(new PropertyChangeListener() {

            public void propertyChange(PropertyChangeEvent evt) {
                if ("windowMoved".equals(evt.getPropertyName())) {
                    calculateBounds();
                }
            }
        });
    }

    private void calculateBounds() {
        GraphicsConfiguration gc = getGraphicsConfiguration();
        Rectangle screenBounds = gc.getBounds();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(gc);
        screenInsets.bottom = Math.max(screenInsets.bottom, 1);
        int maxWidth = screenBounds.width - (screenInsets.left + screenInsets.right);
        int maxHeight = screenBounds.height - (screenInsets.top + screenInsets.bottom);
        setTitle("Maximize-Test -- ScreenWidth = " + screenBounds.width + " ScreenHeight = " + screenBounds.height + " -- MaxWidth = " + maxWidth + " MaxHeight = " + maxHeight);
    }

    public void maximize() {
        // Calculate the maximum bounds
        GraphicsConfiguration gc = getGraphicsConfiguration();
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(gc);
        Rectangle screenBounds = gc.getBounds();
        int x = Math.max(0, screenInsets.left);
        int y = Math.max(0, screenInsets.top);
        int w = screenBounds.width - (screenInsets.left + screenInsets.right);
        int h = screenBounds.height - (screenInsets.top + screenInsets.bottom);
        // Keep taskbar visible.
        // A call to setMaximizedBounds may cause an invalid frame size on multi screen environments
        // see: http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=6699851
        // try and error to avoid the setMaximizedBounds bug
        setMaximizedBounds(new Rectangle(x, y, w, h));
        setExtendedState(Frame.MAXIMIZED_BOTH);
        // This should not happen, because at startup application is on first screen where the problem not occurs
        if (getSize().width > screenBounds.width || getSize().height > screenBounds.height) {
            setMaximizedBounds(null);
            int state = getExtendedState();
            // restore to normal size
            setExtendedState(state & ~Frame.MAXIMIZED_BOTH);
            // maximize again
            setExtendedState(Frame.MAXIMIZED_BOTH);
        }

    }
    /**
     * @param args
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(new AeroLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        MaxApp app = new MaxApp();
        app.setVisible(true);
        app.maximize();
    }
}
