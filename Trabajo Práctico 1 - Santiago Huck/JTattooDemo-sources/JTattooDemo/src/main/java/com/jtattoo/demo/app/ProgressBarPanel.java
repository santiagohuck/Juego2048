/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import com.jtattoo.demo.utils.GridBagHelper;
import com.jtattoo.plaf.JTattooUtilities;
import java.awt.*;
import javax.swing.*;

/**
 *
 * @author  Michael Hagen
 */
public class ProgressBarPanel extends JPanel {
    private JProgressBar horStandardBar = null;
    private JProgressBar horTextBar = null;
    private JProgressBar horDisabledBar = null;
    private JProgressBar horColoredBar = null;
    private JProgressBar horIndeterminatedBar = null;
    private JProgressBar verStandardBar = null;
    private JProgressBar verTextBar = null;
    private JProgressBar verDisabledBar = null;
    private JProgressBar verColoredBar = null;
    private JProgressBar verIndeterminatedBar = null;

    public ProgressBarPanel() {
        super(new BorderLayout());
        init();
    }

    private void init() {
        setName("ProgressBar");
        setBorder(BorderFactory.createEmptyBorder(8, 8, 8, 8));
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.add(createHorProgressbars(), BorderLayout.NORTH);
        contentPanel.add(createVerProgressbars(), BorderLayout.CENTER);

        JScrollPane scrollPane = new JScrollPane(contentPanel);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        add(scrollPane, BorderLayout.CENTER);
    }

    public void updateLookAndFeel() {
        if (JTattooUtilities.getJavaVersion() >= 1.4) {
            horIndeterminatedBar.setIndeterminate(false);
            horIndeterminatedBar.setIndeterminate(true);
            verIndeterminatedBar.setIndeterminate(false);
            verIndeterminatedBar.setIndeterminate(true);
        }
    }

    private JPanel createHorProgressbars() {
        GridBagLayout layout = new GridBagLayout();
        JPanel progressBarPanel = new JPanel(layout);
        progressBarPanel.setBorder(BorderFactory.createEmptyBorder(50, 0, 0, 0));
        horStandardBar = new JProgressBar(0, 100);
        horStandardBar.setValue(33);
        horStandardBar.setPreferredSize(new Dimension(140, 16));
        horTextBar = new JProgressBar(0, 100);
        horTextBar.setValue(50);
        horTextBar.setString("50%");
        horTextBar.setStringPainted(true);
        horTextBar.setPreferredSize(new Dimension(140, 16));
        horDisabledBar = new JProgressBar(0, 100);
        horDisabledBar.setValue(66);
        horDisabledBar.setEnabled(false);
        horDisabledBar.setPreferredSize(new Dimension(140, 16));
        horColoredBar = new JProgressBar(0, 100);
        horColoredBar.setValue(50);
        horColoredBar.setString("50%");
        horColoredBar.setStringPainted(true);
        horColoredBar.setPreferredSize(new Dimension(140, 16));
        horColoredBar.setForeground(Color.yellow);
        horColoredBar.setBackground(Color.blue);
        horColoredBar.putClientProperty("selectionForeground", Color.red);
        horColoredBar.putClientProperty("selectionBackground", Color.green);

        if (JTattooUtilities.getJavaVersion() >= 1.4) {
            horIndeterminatedBar = new JProgressBar();
            horIndeterminatedBar.setString("indeterminated");
            horIndeterminatedBar.setStringPainted(true);
            horIndeterminatedBar.setIndeterminate(true);
            horIndeterminatedBar.setPreferredSize(new Dimension(140, 16));
        }

        GridBagHelper.addComponent(progressBarPanel, horStandardBar,       0, 1, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER);
        GridBagHelper.addComponent(progressBarPanel, horTextBar,           0, 2, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER);
        GridBagHelper.addComponent(progressBarPanel, horDisabledBar,       0, 3, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER);
        if (JTattooUtilities.getJavaVersion() >= 1.4) {
            GridBagHelper.addComponent(progressBarPanel, horIndeterminatedBar, 0, 4, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER);
        }
        GridBagHelper.addComponent(progressBarPanel, horColoredBar,       0, 5, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.HORIZONTAL, GridBagConstraints.CENTER);
        return progressBarPanel;
    }

    private JPanel createVerProgressbars() {
        GridBagLayout layout = new GridBagLayout();
        JPanel progressBarPanel = new JPanel(layout);
        verStandardBar = new JProgressBar(0, 100);
        verStandardBar.setValue(33);
        verStandardBar.setPreferredSize(new Dimension(16, 140));
        verStandardBar.setOrientation(SwingConstants.VERTICAL);
        verTextBar = new JProgressBar(0, 100);
        verTextBar.setValue(50);
        verTextBar.setString("50%");
        verTextBar.setStringPainted(true);
        verTextBar.setPreferredSize(new Dimension(16, 140));
        verTextBar.setOrientation(SwingConstants.VERTICAL);
        verDisabledBar = new JProgressBar(0, 100);
        verDisabledBar.setValue(66);
        verDisabledBar.setEnabled(false);
        verDisabledBar.setPreferredSize(new Dimension(16, 140));
        verDisabledBar.setOrientation(SwingConstants.VERTICAL);
        verColoredBar = new JProgressBar(0, 100);
        verColoredBar.setValue(50);
        verColoredBar.setString("50%");
        verColoredBar.setStringPainted(true);
        verColoredBar.setPreferredSize(new Dimension(16, 140));
        verColoredBar.setForeground(Color.yellow);
        verColoredBar.setBackground(Color.blue);
        verColoredBar.putClientProperty("selectionForeground", Color.red);
        verColoredBar.putClientProperty("selectionBackground", Color.green);
        verColoredBar.setOrientation(SwingConstants.VERTICAL);

        if (JTattooUtilities.getJavaVersion() >= 1.4) {
            verIndeterminatedBar = new JProgressBar();
            verIndeterminatedBar.setString("indeterminated");
            verIndeterminatedBar.setStringPainted(true);
            verIndeterminatedBar.setIndeterminate(true);
            verIndeterminatedBar.setPreferredSize(new Dimension(16, 140));
            verIndeterminatedBar.setOrientation(SwingConstants.VERTICAL);
        }

        GridBagHelper.addComponent(progressBarPanel, verStandardBar,       0, 1, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER);
        GridBagHelper.addComponent(progressBarPanel, verTextBar,           1, 1, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER);
        GridBagHelper.addComponent(progressBarPanel, verDisabledBar,       2, 1, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER);
        if (JTattooUtilities.getJavaVersion() >= 1.4) {
            GridBagHelper.addComponent(progressBarPanel, verIndeterminatedBar, 3, 1, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER);
        }
        GridBagHelper.addComponent(progressBarPanel, verColoredBar,       4, 1, 1, 1, 4, 4,  0.0, 0.0, GridBagConstraints.VERTICAL, GridBagConstraints.CENTER);
        return progressBarPanel;
    }

//    private void changeColors() {
//        textBar.setForeground(Color.YELLOW);
//        textBar.setBackground(Color.BLUE);
//        textBar.putClientProperty("selectionForeground", Color.BLACK);
//        textBar.putClientProperty("selectionForeground", Color.RED);
//        textBar.putClientProperty("selectionBackground", Color.BLACK);
//        textBar.putClientProperty("selectionBackground", Color.GREEN);
//
//        indeterminatedBar.setForeground(Color.YELLOW);
//        indeterminatedBar.setBackground(Color.BLUE);
//        indeterminatedBar.putClientProperty("selectionForeground", Color.BLACK);
//        indeterminatedBar.putClientProperty("selectionForeground", Color.RED);
//        indeterminatedBar.putClientProperty("selectionBackground", Color.BLACK);
//        indeterminatedBar.putClientProperty("selectionBackground", Color.GREEN);
//    }
}
