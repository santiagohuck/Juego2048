/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import com.jtattoo.demo.images.ImageHelper;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;

public class TabTest extends JDialog {

    private final JPanel contentPanel = new JPanel();
    private JTabbedPane tabbedPane = null;
    
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        try {
            // Get the look and feel class name
            String laf = "com.jtattoo.plaf.luna.LunaLookAndFeel";

            // Install the look and feel
            UIManager.setLookAndFeel(laf);
            TabTest dialog = new TabTest();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create the dialog.
     */
    public TabTest() {
        setBounds(100, 100, 450, 300);
        
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.X_AXIS));
        tabbedPane = new JTabbedPane(JTabbedPane.LEFT);
        contentPanel.add(tabbedPane);
        JPanel panel = new JPanel();
        tabbedPane.addTab("", ImageHelper.loadImage("movies.png"), panel, null);
        JLabel lblTextOnTab = new JLabel("Text on Tab1");
        panel.add(lblTextOnTab);
        panel = new JPanel();
        tabbedPane.addTab("", ImageHelper.loadImage("movies.png"), panel, null);
        JLabel lblTextOnTab_1 = new JLabel("Text on tab2");
        panel.add(lblTextOnTab_1);
        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
        getContentPane().add(buttonPane, BorderLayout.SOUTH);
        JButton okButton = new JButton("OK");
        okButton.setActionCommand("OK");
        buttonPane.add(okButton);
        getRootPane().setDefaultButton(okButton);
        JButton cancelButton = new JButton("Cancel");
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setVisible(true);
    }
}
