/*
 * Copyright 2002 and later by MH Software-Entwicklung. All rights reserved.
 * Use is subject to license terms.
 */

package com.jtattoo.demo.app;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class ClipDemo extends JFrame {

    public ClipDemo() {
        super("Clipping-Demo");

        setContentPane(new MyPanel());

        // add listeners
        addWindowListener(new WindowAdapter() {

            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });

        // show application
        setLocation(32, 32);
        setSize(320, 240);
        setVisible(true);
    }

    public class MyPanel extends JPanel {

        public void paint(Graphics g) {
            super.paint(g);

            int w = getWidth();
            int h = getHeight();
            int w2 = w / 2;
            int h2 = h / 2;
            int w4 = w / 4;
            int h4 = h / 4;

            Shape savedClip = g.getClip();

            g.setColor(Color.red);
            g.setClip(10, 10, w2, h2);
            g.fillRect(0, 0, w, h);
            g.setClip(savedClip);

            g.setColor(Color.blue);
            g.setClip(w2 - 10, h2 - 10, w2, h2);
            g.fillRect(0, 0, w, h);
            g.setClip(savedClip);

            g.setColor(Color.orange);
            g.setClip(w4, h4, w2, h2);
            g.fillRect(0, 0, w, h);
            g.setClip(savedClip);
        }

    }

    public static void main(String[] args) {
        try {
            new ClipDemo();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } // end main
} // end class ClipDemo