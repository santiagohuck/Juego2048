package juego2048;


import java.util.ArrayList;
import java.util.Random;

public class Tablero {
	
	private Integer MARGEN_IZQUIERDO;
	private Integer MARGEN_DERECHO;
	private Integer MARGEN_SUPERIOR;
	private Integer MARGEN_INFERIOR;
	
	private int cantFilas;
	private int cantColumnas;
	Ficha[][] tablero;
	
	private int puntaje;
	private int puntajeAnterior;
	private ArrayList<Integer> estadoActual;
	
	
	public Tablero(){		
				
		this.cantFilas = 4;
		this.cantColumnas = 4;
		establecerMargenes();
		iniciarTablero();		
		this.puntajeAnterior = -1;
		this.estadoActual = new ArrayList<Integer>(this.cantFilas*this.cantColumnas);
		
	}
	
	private void establecerMargenes(){
		this.MARGEN_IZQUIERDO = 0;
		this.MARGEN_DERECHO = this.cantFilas-1;
		this.MARGEN_SUPERIOR = 0;
		this.MARGEN_INFERIOR = this.cantColumnas-1;
	}
	
	public void iniciarTablero(){
		
		this.tablero = new Ficha[this.cantFilas][this.cantColumnas];
		for (int i=0; i<this.cantFilas;i++){
			for (int j=0; j<this.cantColumnas; j++){
				this.tablero[i][j] = new Ficha(0);
			}
		}
		añadirFicha();
		añadirFicha();
		
	}
	
	public void siguienteTurno(){
		for (int i=0; i<this.cantFilas;i++){
			for (int j=0; j<this.cantColumnas; j++){
				this.tablero[i][j].setFueFusionada(false);
			}
		}
		
		añadirFicha();
		
	}
	
	public String jugada(String direccion){
		
		actualizarPuntajeAnterior();
		movimiento(direccion);
		fusion(direccion);
		movimiento(direccion);
		fusion(direccion);
		actualizarEstadoActual();
		if (finDePartida()=="Game Over"){
			return "Game Over";
		}
		if (finDePartida()=="Victoria"){
			return "Victoria";
		}
		return "Con vida";	
		
	}
	
	private void actualizarPuntajeAnterior(){
		this.puntajeAnterior = this.puntaje;
	}
	
	private void actualizarEstadoActual(){
		this.estadoActual.clear();
		for (int i=0; i<this.cantFilas;i++){
			for (int j=0; j<this.cantColumnas; j++){
				this.estadoActual.add(this.tablero[i][j].getValorActual());
			}
		}
	}
	
	
	private void añadirFicha(){
		Random random = new Random();
		int[] x = {2,4};
		ArrayList<Tupla<Integer,Integer>> posibles = new ArrayList<Tupla<Integer,Integer>>();
		for (int i=0; i<4;i++){
			for (int j=0; j<4;j++){
				if (this.tablero[i][j].getValorActual()==0)	
					posibles.add(new Tupla<Integer,Integer>(i,j));
			}
		}
		if (posibles.isEmpty()==false){
			int y = random.nextInt(posibles.size());
			this.tablero[posibles.get(y).getElem1()][posibles.get(y).getElem2()].setValorActual(x[random.nextInt(2)]);
			this.tablero[posibles.get(y).getElem1()][posibles.get(y).getElem2()].setFueFusionada(false);
		}		
	}
	
	public void movimiento(String direccion){
		switch(direccion){
			case "Derecha":
				for (int k=0; k<this.cantFilas-1;k++){
					for (int i=0; i<=this.cantFilas-1;i++){
						for (int j=0; j<=this.cantColumnas-1; j++){
							if (j+1<=this.MARGEN_DERECHO){
								mover(this.tablero[i][j],this.tablero[i][j+1]);
							}
						}
					}
				}
				break;
			case "Izquierda":
				for (int k=0; k<this.cantFilas-1;k++){
					for (int i=0; i<=this.cantFilas-1;i++){
						for (int j=this.cantColumnas-1; j>=0; j--){
							if (j-1>=this.MARGEN_IZQUIERDO){
								mover(this.tablero[i][j],this.tablero[i][j-1]);
							}
						}
					}
				}
				break;
			case "Abajo":
				for (int k=0; k<this.cantFilas-1;k++){
					for (int i=0; i<=this.cantFilas-1;i++){
						for (int j=0; j<=this.cantColumnas-1; j++){
							if (i+1<=this.MARGEN_INFERIOR){
								mover(this.tablero[i][j],this.tablero[i+1][j]);
							}
						}
					}
				}
				break;
			case "Arriba":
				for (int k=0; k<this.cantFilas-1;k++){
					for (int i=this.cantFilas-1; i>=0;i--){
						for (int j=0; j<=this.cantColumnas-1; j++){
							if (i-1>=this.MARGEN_SUPERIOR){
								mover(this.tablero[i][j],this.tablero[i-1][j]);
							}
						}
					}
				}
				break;
		}		
	}
	
	private void mover(Ficha aMover, Ficha aqui){
			if (aqui.getValorActual()==0){
				aqui.setValorActual(aMover.getValorActual());
				aMover.setValorActual(0);
			}				
	}	
	
	public void fusion(String direccion){
		switch(direccion){
		case "Derecha":
			for (int k=0; k<this.cantFilas-1;k++){
				for (int i=0; i<=this.cantFilas-1;i++){
					for (int j=0; j<=this.cantColumnas-1; j++){
						if (j+1<=this.MARGEN_DERECHO){
							fusionar(this.tablero[i][j],this.tablero[i][j+1]);
						}
					}
				}
			}
			break;
		case "Izquierda":
			for (int k=0; k<this.cantFilas-1;k++){
				for (int i=0; i<=this.cantFilas-1;i++){
					for (int j=this.cantColumnas-1; j>=0; j--){
						if (j-1>=this.MARGEN_IZQUIERDO){
							fusionar(this.tablero[i][j],this.tablero[i][j-1]);
						}
					}
				}
			}
			break;
		case "Abajo":
			for (int k=0; k<this.cantFilas-1;k++){
				for (int i=0; i<=this.cantFilas-1;i++){
					for (int j=0; j<=this.cantColumnas-1; j++){
						if (i+1<=this.MARGEN_INFERIOR){
							fusionar(this.tablero[i][j],this.tablero[i+1][j]);
						}
					}
				}
			}
			break;
		case "Arriba":
			for (int k=0; k<this.cantFilas-1;k++){
				for (int i=this.cantFilas-1; i>=0;i--){
					for (int j=0; j<=this.cantColumnas-1; j++){
						if (i-1>=this.MARGEN_SUPERIOR){
							fusionar(this.tablero[i][j],this.tablero[i-1][j]);
						}
					}
				}
			}
			break;
		}		
	}

	private void fusionar(Ficha seVa, Ficha seFusiona) {
		if (seVa.getFueFusionada()==false && seFusiona.getFueFusionada()==false){
			if (seVa.getValorActual()>0){
				if (seVa.getValorActual()==seFusiona.getValorActual()){
					seFusiona.fusionar();
					this.puntaje +=seVa.getValorActual()*2;
					seVa.setValorActual(0);
					seVa.setFueFusionada(true);
				}
			}
		}
	}

	public ArrayList<Ficha> devolverEstadoActual() {
		ArrayList<Ficha> aux = new ArrayList<Ficha>();
		for (int i=0; i<this.cantFilas;i++){
			for (int j=0; j<this.cantColumnas; j++){
				aux.add(this.tablero[i][j]);
			}
		}
		return aux;
	}

	public Integer devolverPuntaje() {
		return this.puntaje;
	}
	
	public boolean[] posibleMovimiento(){
		
		boolean[] posibilidades = new boolean[2];
		for (int i=0; i<2;i++){
			posibilidades[i] = false;
		}
		
		//Izquierda-Derecha:				
			for (int i=0; i<=this.cantFilas-1;i++){
				for (int j=0; j<=this.cantColumnas-1; j++){
					if (j+1<=this.MARGEN_DERECHO){
						posibilidades[0] = posibilidades[0] || esPosible(this.tablero[i][j],this.tablero[i][j+1]);
					}
				}
			}			
		
		//Abajo-Arriba:				
			for (int i=0; i<=this.cantFilas-1;i++){
				for (int j=0; j<=this.cantColumnas-1; j++){
					if (i+1<=this.MARGEN_INFERIOR){
						posibilidades[1] = posibilidades[1] || esPosible(this.tablero[i][j],this.tablero[i+1][j]);
					}
				}
			}				
		
			return posibilidades;
	}
	
	private boolean esPosible(Ficha uno, Ficha dos){
			if (uno.getValorActual()==dos.getValorActual() && uno.getValorActual()!=0){
				return true;
			}				
			return false;
	}
	
	public String finDePartida(){
		boolean[] aux = this.posibleMovimiento();
		if (this.puntaje == this.puntajeAnterior && !this.estadoActual.contains(0) && aux[0]==false && aux[1]==false){
			return "Game Over";
		}else if (this.estadoActual.contains(2048)){
			return "Victoria";
		}else{
			return "Con vida";
		}		
	}	
	
	
	@Override
	public String toString(){
		StringBuilder aux = new StringBuilder();
		for (int i=0; i<4;i++){
			for (int j=0; j<4;j++){
				aux.append(this.tablero[i][j].getValorActual()+" " );
			}
			aux.append("\n");
		}
		return aux.toString();
	}
	

}
