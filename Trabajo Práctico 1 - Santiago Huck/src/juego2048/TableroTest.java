package juego2048;

import static org.junit.Assert.*;

import org.junit.Test;

public class TableroTest {

	Tablero tablero = new Tablero();
	
	public TableroTest(){
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				this.tablero.tablero[i][j].setValorActual(0);
				}
		}
	}
	
	
	@Test
	public void tableroNuevo() {
		assertTrue(this.tablero.tablero[0][0].getValorActual()==0);
		assertTrue(this.tablero.tablero[3][3].getValorActual()==0);
	}
	
	@Test
	public void setearValorFicha() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[0][0].setFueFusionada(false);
		assertTrue(this.tablero.tablero[0][0].getValorActual()==2);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==0);
	}
	
	@Test
	public void moverDerecha() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[0][2].setValorActual(4);
		System.out.println("Tablero Inicial :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		
		this.tablero.movimiento("Derecha");
		System.out.println("Mover Derecha :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		assertTrue(this.tablero.tablero[0][2].getValorActual()==2);
		assertTrue(this.tablero.tablero[0][3].getValorActual()==4);
		System.out.println("");
		
	}
	
	@Test
	public void moverIzquierda() {
		this.tablero.tablero[0][1].setValorActual(2);
		this.tablero.tablero[0][3].setValorActual(4);
		System.out.println("Tablero Inicial :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		this.tablero.movimiento("Izquierda");
		System.out.println("Mover Izquierda :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.println("");
		assertTrue(this.tablero.tablero[0][0].getValorActual()==2);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==4);
		
	}
	
	@Test
	public void prueba(){
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[0][1].setValorActual(0);
		this.tablero.tablero[0][2].setValorActual(0);
		this.tablero.tablero[0][3].setValorActual(2);
		this.tablero.tablero[1][0].setValorActual(8);
		this.tablero.tablero[1][1].setValorActual(2);
		this.tablero.tablero[1][2].setValorActual(2);
		this.tablero.tablero[1][3].setValorActual(0);
		this.tablero.tablero[2][0].setValorActual(0);
		this.tablero.tablero[2][1].setValorActual(16);
		this.tablero.tablero[2][2].setValorActual(4);
		this.tablero.tablero[2][3].setValorActual(4);
		this.tablero.tablero[3][0].setValorActual(0);
		this.tablero.tablero[3][1].setValorActual(0);
		this.tablero.tablero[3][2].setValorActual(0);
		this.tablero.tablero[3][3].setValorActual(2);
		System.out.println("Tablero Inicial :");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		System.out.println("");
		this.tablero.fusion("Izquierda");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}System.out.println("");
		assertTrue(this.tablero.tablero[0][0].getValorActual()==2);
		assertTrue(this.tablero.tablero[0][3].getValorActual()==2);
		this.tablero.movimiento("Izquierda");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}System.out.println("");
		assertTrue(this.tablero.tablero[0][0].getValorActual()==2);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==2);
		this.tablero.fusion("Izquierda");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		assertTrue(this.tablero.tablero[0][0].getValorActual()==4);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==0);
	}
	
	
	@Test
	public void moverAbajo() {
		this.tablero.tablero[0][1].setValorActual(2);
		this.tablero.tablero[0][3].setValorActual(4);
		System.out.println("Tablero Inicial :");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}		
		this.tablero.movimiento("Abajo");
		System.out.println("Mover Abajo :");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		System.out.println("");
		assertTrue(this.tablero.tablero[3][1].getValorActual()==2);
		assertTrue(this.tablero.tablero[3][3].getValorActual()==4);
		
	}
	
	@Test
	public void moverArriba() {
		this.tablero.tablero[3][1].setValorActual(2);
		this.tablero.tablero[3][3].setValorActual(4);
		System.out.println("Tablero Inicial :");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		this.tablero.movimiento("Arriba");
		System.out.println("Mover Arriba :");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		System.out.println("");
		assertTrue(this.tablero.tablero[0][1].getValorActual()==2);
		assertTrue(this.tablero.tablero[0][3].getValorActual()==4);
		
	}
	
	@Test
	public void fusionIzq() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[0][1].setValorActual(2);
		this.tablero.tablero[0][2].setValorActual(2);
		this.tablero.tablero[0][3].setValorActual(2);
		System.out.println("Tablero Inicial :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		this.tablero.fusion("Izquierda");
		System.out.println("Fusion Izquierda :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		assertTrue(this.tablero.tablero[0][0].getValorActual()==4);
		assertTrue(this.tablero.tablero[0][2].getValorActual()==4);
		this.tablero.movimiento("Izquierda");
		System.out.println("Mover Izquierda :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		assertTrue(this.tablero.tablero[0][0].getValorActual()==4);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==4);
		this.tablero.fusion("Izquierda");
		System.out.println("Fusion Izquierda en mismo turno:");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		assertTrue(this.tablero.tablero[0][0].getValorActual()==4);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==4);
		this.tablero.siguienteTurno();
		this.tablero.fusion("Izquierda");
		System.out.println("Fusion Izquierda en el turno siguiente:");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.println("");
		assertTrue(this.tablero.tablero[0][0].getValorActual()==8);
		assertTrue(this.tablero.tablero[0][1].getValorActual()==0);
		
		
	}
	
	@Test
	public void fusionDer() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[0][1].setValorActual(2);
		this.tablero.tablero[0][2].setValorActual(8);
		this.tablero.tablero[0][3].setValorActual(8);
		System.out.println("Tablero Inicial :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		this.tablero.fusion("Derecha");
		System.out.println("Fusion Derecha :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.println("");
		assertTrue(this.tablero.tablero[0][1].getValorActual()==4);
		assertTrue(this.tablero.tablero[0][3].getValorActual()==16);
		
	}
	
	@Test
	public void fusionAba() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[1][0].setValorActual(2);
		this.tablero.tablero[0][1].setValorActual(8);
		this.tablero.tablero[1][1].setValorActual(8);
		System.out.println("Tablero Inicial :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.print(this.tablero.tablero[1][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[1][3].getValorActual());
		this.tablero.fusion("Abajo");
		System.out.println("Fusion Abajo :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.print(this.tablero.tablero[1][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[1][3].getValorActual());
		System.out.println("");
		assertTrue(this.tablero.tablero[1][0].getValorActual()==4);
		assertTrue(this.tablero.tablero[1][1].getValorActual()==16);
		
		
	}
	
	@Test
	public void fusionArr() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[1][0].setValorActual(2);
		this.tablero.tablero[0][1].setValorActual(8);
		this.tablero.tablero[1][1].setValorActual(8);
		System.out.println("Tablero Inicial :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.print(this.tablero.tablero[1][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[1][3].getValorActual());
		this.tablero.fusion("Arriba");
		System.out.println("Fusion Arriba :");
		System.out.print(this.tablero.tablero[0][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[0][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[0][3].getValorActual());
		System.out.print(this.tablero.tablero[1][0].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][1].getValorActual()+" - ");
		System.out.print(this.tablero.tablero[1][2].getValorActual()+" - ");
		System.out.println(this.tablero.tablero[1][3].getValorActual());
		System.out.println("");
		assertTrue(this.tablero.tablero[0][1].getValorActual()==16);
		assertTrue(this.tablero.tablero[0][0].getValorActual()==4);
		
		
	}
	
	@Test
	public void jugada() {
		this.tablero.tablero[0][0].setValorActual(2);
		this.tablero.tablero[0][1].setValorActual(0);
		this.tablero.tablero[0][2].setValorActual(0);
		this.tablero.tablero[0][3].setValorActual(2);
		this.tablero.tablero[1][0].setValorActual(8);
		this.tablero.tablero[1][1].setValorActual(2);
		this.tablero.tablero[1][2].setValorActual(2);
		this.tablero.tablero[1][3].setValorActual(0);
		this.tablero.tablero[2][0].setValorActual(0);
		this.tablero.tablero[2][1].setValorActual(16);
		this.tablero.tablero[2][2].setValorActual(4);
		this.tablero.tablero[2][3].setValorActual(4);
		this.tablero.tablero[3][0].setValorActual(0);
		this.tablero.tablero[3][1].setValorActual(0);
		this.tablero.tablero[3][2].setValorActual(0);
		this.tablero.tablero[3][3].setValorActual(2);
		System.out.println("Tablero Inicial :");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		this.tablero.jugada("Izquierda");
		System.out.println("Izquierda:");
		for (int i=0; i<4;i++){
			for (int j=0; j<4; j++){
				System.out.print(this.tablero.tablero[i][j].getValorActual()+" - ");
			}
			System.out.println("");			
		}
		System.out.println("");
		assertTrue(this.tablero.tablero[0][0].getValorActual()==4);
		
	}

}
