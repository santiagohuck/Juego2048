package juego2048;

public class Ficha {
	
	private int valorActual;
	private boolean fueFusionada;
	
	Ficha(int valor){
		
		this.valorActual = valor;
		this.fueFusionada = false;
		
	}
	
	public void setValorActual(int nuevoValor){
		this.valorActual = nuevoValor;
	}
	
	
	public String getValorActualString(){
		if (this.valorActual!=0){
			return this.valorActual+"";
		}
		return "";
	}
	
	public int getValorActual(){
		return this.valorActual;
	}
	
	
	public void setFueFusionada(boolean estado){
		this.fueFusionada = estado;
	}
	
	public boolean getFueFusionada(){
		return this.fueFusionada;
	}
	
	public void fusionar(){
		this.setValorActual(this.valorActual*2);
		this.fueFusionada = true;
	}
	

}
