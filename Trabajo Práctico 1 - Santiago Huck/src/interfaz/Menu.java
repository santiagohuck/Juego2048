package interfaz;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.UIManager;

import java.awt.Font;

import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.SwingConstants;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Menu {

	private JFrame ventana;
	private JButton botonJugar, botonRecords, botonSalir;
	private Records records;
	private Juego juego;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu menu = new Menu();
					menu.ventana.setVisible(true);										
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public Menu() {
		
		try{
			UIManager.setLookAndFeel("com.jtattoo.plaf.texture.TextureLookAndFeel");
		}catch (Exception e){
			throw new IllegalArgumentException("UI no encontrada");
		}
		
		initializar();
		
		
	}
	
	public void setVisible(boolean visibilidad){
		ventana.setVisible(visibilidad);
	}
	
	private void initializar() {
		iniciarVentana();
		ponerTitulo();
		iniciarBotones();
		
	}
		
	private void iniciarVentana(){
		ventana = new JFrame();
		ventana.setTitle("2048");
		ventana.setBounds(500, 150, 340, 500);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);	
		records = new Records(this);
		juego = new Juego(this,this.records);
	}
	
	private void ponerTitulo(){
		JLabel titulo = new JLabel("2048");
		titulo.setFocusable(false);
		titulo.setForeground(new Color(153, 51, 0));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		titulo.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 60));
		titulo.setBounds(47, 32, 239, 72);
		ventana.getContentPane().add(titulo);
	}
	
	private void iniciarBotones(){
		botonJugar = new JButton("Jugar");
		botonJugar.setFocusable(false);
		botonJugar.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		botonJugar.setBounds(80, 133, 176, 90);
		botonJugar.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonJugar);
		botonJugar.addActionListener(new VentanaJuego());
		
		botonRecords = new JButton("Records");
		botonRecords.setFocusable(false);
		botonRecords.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		botonRecords.setBounds(80, 234, 176, 90);
		botonRecords.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonRecords);
		botonRecords.addActionListener(new VentanaRecords());
		
		botonSalir = new JButton("Salir");
		botonSalir.setFocusable(false);
		botonSalir.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 30));
		botonSalir.setBounds(80, 339, 176, 90);
		botonSalir.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonSalir);
		botonSalir.addActionListener(new CloseListener());
				
	}		
	
	private class VentanaRecords implements ActionListener{
		    @Override
		    public void actionPerformed(ActionEvent e) {
				records.setVisible(true);
				ventana.setVisible(false);
		    }
	}
	
	private class VentanaJuego implements ActionListener{
	    @Override
	    public void actionPerformed(ActionEvent e) {
	    	juego.nuevaPartida();
			juego.setVisible(true);
			ventana.setVisible(false);
	    }
	}
	
	private class CloseListener implements ActionListener{
		  
			@Override
		    public void actionPerformed(ActionEvent e) {
		        System.exit(0);
		    }
	}
	
}
