package interfaz;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.stream.Stream;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
























import juego2048.Tupla;

import java.awt.Font;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Records {

	private JFrame ventana;
	private JTable tablaRecords;
	private JButton botonVolver;
	private JScrollPane panel;
	private JScrollBar scrollBar;
	private ArrayList<Tupla<String,String>> listaRecords;
	private Menu menu;	
	private File nombres;
	private File puntajes;
	private FileWriter n;
	private FileWriter p;
	private Stream<String> stream;
	private Stream<String> stream2;

	public Records(Menu menu){		
		initializar();
		this.menu = menu;
		
		this.nombres = new File("Recursos\\nombres.txt");
		this.puntajes = new File("Recursos\\puntajes.txt");
		if (!this.nombres.exists()){
			try {
				this.nombres.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (!this.puntajes.exists()){
			try {
				this.puntajes.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void setVisible(boolean visibilidad){
		ventana.setVisible(visibilidad);
	}

	
	private void initializar() {
		iniciarVentana();
		iniciarBotones();
		iniciarPanel();
		
	}
	
	private void iniciarVentana(){
		
		ventana = new JFrame();
		ventana.setBounds(500, 150, 340, 500);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);
		
	}
	
	private void iniciarBotones(){
		
		botonVolver = new JButton("Volver");
		botonVolver.setFocusable(false);
		botonVolver.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 16));
		botonVolver.setBounds(203, 412, 111, 39);
		botonVolver.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonVolver);
		botonVolver.addActionListener(new Volver());
			
	}
	
	private void iniciarPanel(){
		
		panel = new JScrollPane();
		panel.setBounds(10, 11, 304, 390);
		ventana.getContentPane().add(panel);

		this.listaRecords = new ArrayList<Tupla<String,String>>();
		
		tablaRecords = new JTable();
		tablaRecords.setForeground(new Color(153, 51, 0));
		tablaRecords.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 11));
		tablaRecords.setEnabled(false);
		tablaRecords.setFocusable(false);
		tablaRecords.setModel(modeloTabla(listaRecords));
		panel.setViewportView(tablaRecords);
		
		scrollBar = new JScrollBar();
		scrollBar.setBackground(new Color(153, 51, 0));
		panel.setRowHeaderView(scrollBar);
		
	}
	
	private DefaultTableModel modeloTabla(ArrayList<Tupla<String,String>> listaRecords){
		DefaultTableModel tableModel = new DefaultTableModel();
		tableModel.addColumn("Nombre");
		tableModel.addColumn("Puntaje");
		
		ArrayList<String> list1 = new ArrayList<String>();	
		ArrayList<Integer> list2 = new ArrayList<Integer>();	
		ArrayList<Tupla<String,Integer>> list3 = new ArrayList<Tupla<String,Integer>>();	
		try {
			stream = Files.lines(Paths.get("Recursos\\nombres.txt"),Charset.forName("Cp1252"));
			stream2 = Files.lines(Paths.get("Recursos\\puntajes.txt"),Charset.forName("Cp1252"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		Iterator<String> iter = stream.iterator();
		Iterator<String> iter2 = stream2.iterator();
		
		while(iter.hasNext()){
			list1.add(iter.next());
		}
		while(iter2.hasNext()){
			list2.add(Integer.parseInt(iter2.next()));
		}		        
		
		for (int i=0; i<list1.size();i++){
			list3.add(new Tupla<String,Integer>(list1.get(i),list2.get(i)));
		}
		
		Comparator<Tupla<String, Integer>> comparator = new Comparator<Tupla<String, Integer>>()
			    {
			        public int compare(Tupla<String, Integer> tuplaA, Tupla<String, Integer> tuplaB)
			        {
			            return tuplaB.getElem2().compareTo(tuplaA.getElem2());
			        }

			    };
			    
			    
		Collections.sort(list3, comparator);
			    
		for (int i=0; i<list3.size();i++){
			String nombre = list3.get(i).getElem1();
			Integer puntaje = list3.get(i).getElem2();
			tableModel.addRow(new String[] {nombre.toUpperCase(),puntaje+""});
		}
		return tableModel;	
		
	}
	
	public void actualizarTabla(){
		tablaRecords.setModel(modeloTabla(listaRecords));
	}
	
	public void registrarRecord(String nombre, Integer puntaje){
		
		try {
			n = new FileWriter(nombres,true);
			p = new FileWriter(puntajes,true);
			n.write(nombre);
			n.write(System.lineSeparator());
			p.write(puntaje+"");
			p.write(System.lineSeparator());
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			this.n.close();
			this.p.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private class Volver implements ActionListener{
	    @Override
	    public void actionPerformed(ActionEvent e) {
	        ventana.setVisible(false);
	        menu.setVisible(true);
	    }
	}

}
