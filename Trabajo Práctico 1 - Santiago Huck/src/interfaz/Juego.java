package interfaz;

import juego2048.*;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import java.awt.Font;

import javax.swing.border.LineBorder;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JButton;
import java.util.ArrayList;
import java.io.File;

public class Juego {

	private JFrame ventana;		
	private JLabel comoJugar1, comoJugar2, comoJugar3, comoJugar4, lbl2048;
	private JLabel puntajeNum, puntuacion;
	private Integer puntaje;
	private JButton botonVolver;
	
	private Tablero tablero;
	private JLabel x0y0, x0y1, x0y2, x0y3;
	private JLabel x1y0, x1y1, x1y2, x1y3;
	private JLabel x2y0, x2y1, x2y2, x2y3;
	private JLabel x3y0, x3y1, x3y2, x3y3;
	private ArrayList<JLabel> fichas;	
	
	private Menu menu;
	private Records records;	
	private GameOver gameOver;

		
	public Juego(Menu menu,Records records) 
	{
		
		this.tablero = new Tablero();
		this.fichas = new ArrayList<JLabel>(16);
		
		this.menu = menu;
		this.records = records;
		this.gameOver = new GameOver(this.menu,this.records);
		
		this.tablero.iniciarTablero();
		inicializar();
		actualizarFichas();
		
				
	}
	
	public void setVisible(boolean visibilidad){
		ventana.setVisible(visibilidad);
	}

	private void inicializar() {
		
		iniciarVentana();
		iniciarFichas();
		iniciarBotones();
		iniciarTextos();
		
	}
	
	private void iniciarVentana(){
		
		ventana = new JFrame();
		ventana.setBounds(500, 150, 365, 528);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);
		ventana.addKeyListener(new teclaPresionada());
		this.ventana.getKeyListeners();
		this.ventana.setFocusable(true);
		this.ventana.setFocusTraversalKeysEnabled(false);
		
	}
	
	private void iniciarBotones(){
		
		botonVolver = new JButton("Volver");
		botonVolver.setFocusable(false);
		botonVolver.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 11));
		botonVolver.setBounds(137, 461, 89, 23);
		botonVolver.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonVolver);
		botonVolver.addActionListener(new Volver());
		
		
	}
	
	private void iniciarTextos(){
		
		puntuacion = new JLabel("Puntuacion: ");
		puntuacion.setForeground(new Color(153, 51, 0));
		puntuacion.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 15));
		puntuacion.setBounds(154, 10, 130, 29);
		ventana.getContentPane().add(puntuacion);
		
		puntajeNum = new JLabel("0");
		puntajeNum.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 15));
		puntajeNum.setHorizontalAlignment(SwingConstants.CENTER);
		puntajeNum.setForeground(new Color(153, 51, 0));
		puntajeNum.setBackground(Color.PINK);
		puntajeNum.setBounds(276, 15, 64, 19);
		ventana.getContentPane().add(puntajeNum);
		
		lbl2048 = new JLabel("Suma 2048!");
		lbl2048.setForeground(new Color(153, 51, 0));
		lbl2048.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 15));
		lbl2048.setBounds(24, 11, 113, 28);
		ventana.getContentPane().add(lbl2048);
		
		comoJugar1 = new JLabel("\u00BFC\u00F3mo jugar?");
		comoJugar1.setHorizontalAlignment(SwingConstants.CENTER);
		comoJugar1.setForeground(new Color(153, 51, 0));
		comoJugar1.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 14));
		comoJugar1.setBounds(74, 389, 210, 23);
		ventana.getContentPane().add(comoJugar1);
		
		comoJugar2 = new JLabel("Usa las flechas para mover las fichas.");
		comoJugar2.setForeground(new Color(153, 51, 0));
		comoJugar2.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 10));
		comoJugar2.setHorizontalAlignment(SwingConstants.CENTER);
		comoJugar2.setBounds(24, 408, 316, 23);
		ventana.getContentPane().add(comoJugar2);
		
		comoJugar3 = new JLabel("Cuando dos fichas con el mismo n\u00FAmero ");
		comoJugar3.setForeground(new Color(153, 51, 0));
		comoJugar3.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 10));
		comoJugar3.setHorizontalAlignment(SwingConstants.CENTER);
		comoJugar3.setBounds(46, 423, 270, 23);
		ventana.getContentPane().add(comoJugar3);
		
		comoJugar4 = new JLabel("se choquen, \u00E9stas se fusionar\u00E1n!");
		comoJugar4.setForeground(new Color(153, 51, 0));
		comoJugar4.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 10));
		comoJugar4.setHorizontalAlignment(SwingConstants.CENTER);
		comoJugar4.setBounds(46, 442, 270, 14);
		ventana.getContentPane().add(comoJugar4);	
	}
	
	private void iniciarFichas(){
		
		x0y0 = new JLabel("");
		x0y0.setFont(new Font("Tahoma", Font.BOLD, 20));
		x0y0.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x0y0.setHorizontalAlignment(SwingConstants.CENTER);
		x0y0.setBounds(10, 50, 75, 75);
		ventana.getContentPane().add(x0y0);		
		this.fichas.add(x0y0);
		
		x0y1 = new JLabel("");
		x0y1.setHorizontalAlignment(SwingConstants.CENTER);
		x0y1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x0y1.setFont(new Font("Tahoma", Font.BOLD, 20));
		x0y1.setBounds(95, 50, 75, 75);
		ventana.getContentPane().add(x0y1);
		this.fichas.add(x0y1);
		
		x0y2 = new JLabel("");
		x0y2.setHorizontalAlignment(SwingConstants.CENTER);
		x0y2.setFont(new Font("Tahoma", Font.BOLD, 20));
		x0y2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x0y2.setBounds(180, 50, 75, 75);
		ventana.getContentPane().add(x0y2);
		this.fichas.add(x0y2);
		
		x0y3 = new JLabel("");
		x0y3.setHorizontalAlignment(SwingConstants.CENTER);
		x0y3.setFont(new Font("Tahoma", Font.BOLD, 20));
		x0y3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x0y3.setBounds(265, 50, 75, 75);
		ventana.getContentPane().add(x0y3);
		this.fichas.add(x0y3);
		
		x1y0 = new JLabel("");
		x1y0.setHorizontalAlignment(SwingConstants.CENTER);
		x1y0.setFont(new Font("Tahoma", Font.BOLD, 20));
		x1y0.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x1y0.setBounds(10, 132, 75, 75);
		ventana.getContentPane().add(x1y0);
		this.fichas.add(x1y0);
		
		x1y1 = new JLabel("");
		x1y1.setHorizontalAlignment(SwingConstants.CENTER);
		x1y1.setFont(new Font("Tahoma", Font.BOLD, 20));
		x1y1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x1y1.setBounds(95, 132, 75, 75);
		ventana.getContentPane().add(x1y1);
		this.fichas.add(x1y1);
		
		x1y2 = new JLabel("");
		x1y2.setHorizontalAlignment(SwingConstants.CENTER);
		x1y2.setFont(new Font("Tahoma", Font.BOLD, 20));
		x1y2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x1y2.setBounds(180, 132, 75, 75);
		ventana.getContentPane().add(x1y2);
		this.fichas.add(x1y2);
		
		x1y3 = new JLabel("");
		x1y3.setHorizontalAlignment(SwingConstants.CENTER);
		x1y3.setFont(new Font("Tahoma", Font.BOLD, 20));
		x1y3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x1y3.setBounds(265, 132, 75, 75);
		ventana.getContentPane().add(x1y3);
		this.fichas.add(x1y3);
		
		x2y0 = new JLabel("");
		x2y0.setHorizontalAlignment(SwingConstants.CENTER);
		x2y0.setFont(new Font("Tahoma", Font.BOLD, 20));
		x2y0.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x2y0.setBounds(10, 214, 75, 75);
		ventana.getContentPane().add(x2y0);
		this.fichas.add(x2y0);
		
		x2y1 = new JLabel("");
		x2y1.setHorizontalAlignment(SwingConstants.CENTER);
		x2y1.setFont(new Font("Tahoma", Font.BOLD, 20));
		x2y1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x2y1.setBounds(95, 214, 75, 75);
		ventana.getContentPane().add(x2y1);
		this.fichas.add(x2y1);
		
		x2y2 = new JLabel("");
		x2y2.setHorizontalAlignment(SwingConstants.CENTER);
		x2y2.setFont(new Font("Tahoma", Font.BOLD, 20));
		x2y2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x2y2.setBounds(180, 214, 75, 75);
		ventana.getContentPane().add(x2y2);
		this.fichas.add(x2y2);
		
		
		x2y3 = new JLabel("");
		x2y3.setHorizontalAlignment(SwingConstants.CENTER);
		x2y3.setFont(new Font("Tahoma", Font.BOLD, 20));
		x2y3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x2y3.setBounds(265, 214, 75, 75);
		ventana.getContentPane().add(x2y3);
		this.fichas.add(x2y3);
		
		x3y0 = new JLabel("");
		x3y0.setHorizontalAlignment(SwingConstants.CENTER);
		x3y0.setFont(new Font("Tahoma", Font.BOLD, 20));
		x3y0.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x3y0.setBounds(10, 300, 75, 75);
		ventana.getContentPane().add(x3y0);
		this.fichas.add(x3y0);
		
		x3y1 = new JLabel("");
		x3y1.setHorizontalAlignment(SwingConstants.CENTER);
		x3y1.setFont(new Font("Tahoma", Font.BOLD, 20));
		x3y1.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x3y1.setBounds(95, 300, 75, 75);
		ventana.getContentPane().add(x3y1);
		this.fichas.add(x3y1);
		
		x3y2 = new JLabel("");
		x3y2.setHorizontalAlignment(SwingConstants.CENTER);
		x3y2.setFont(new Font("Tahoma", Font.BOLD, 20));
		x3y2.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x3y2.setBounds(180, 300, 75, 75);
		ventana.getContentPane().add(x3y2);
		this.fichas.add(x3y2);
		
		x3y3 = new JLabel("");
		x3y3.setHorizontalAlignment(SwingConstants.CENTER);
		x3y3.setFont(new Font("Tahoma", Font.BOLD, 20));
		x3y3.setBorder(new LineBorder(new Color(0, 0, 0), 2));
		x3y3.setBounds(265, 300, 75, 75);
		ventana.getContentPane().add(x3y3);
		this.fichas.add(x3y3);
		
		
	}
	
	public void jugar(String movimiento){
		String aux = tablero.jugada(movimiento);
		if (aux == "Game Over"){
			sonido("Game Over");
			gameOver.setPuntaje(this.puntaje);
			gameOver.setResultado("Game Over");
			gameOver.setVisible(true);
			this.ventana.setVisible(false);
		}
		if (aux == "Victoria"){
			sonido("Victoria");
			gameOver.setPuntaje(this.puntaje);
			gameOver.setResultado("Victoria");
			gameOver.setVisible(true);
			this.ventana.setVisible(false);			
		}
		tablero.siguienteTurno();
	}
		
	
	private void actualizarFichas(){
		for (int i=0; i<16;i++){
			this.fichas.get(i).setIcon(new ImageIcon("bin\\ficha base.png"));
										
		}
		actualizarColor();
		actualizarPuntaje();
	}
	
	private void actualizarColor(){
		ArrayList<Ficha> matriz = this.tablero.devolverEstadoActual();
		for (int i =0;i<16;i++){	
			String valor = matriz.get(i).getValorActualString();
			if (valor.equals("2")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 2.png"));
			}else if (valor.equals("4")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 4.png"));
			}else if (valor.equals("8")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 8.png"));
			}else if (valor.equals("16")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 16.png"));
			}else if (valor.equals("32")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 32.png"));
			}else if (valor.equals("64")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 64.png"));
			}else if (valor.equals("128")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 128.png"));
			}else if (valor.equals("256")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 256.png"));
			}else if (valor.equals("512")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 512.png"));
			}else if (valor.equals("1024")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 1024.png"));
			}else if (valor.equals("2048")){
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha 2048.png"));
			}else {
				this.fichas.get(i).setIcon(new ImageIcon("Recursos\\ficha base.png"));
			}
		}
	}
	
	private void actualizarPuntaje(){
		this.puntaje = tablero.devolverPuntaje();
		this.puntajeNum.setText(this.puntaje+"");
	}
	

	private class teclaPresionada implements KeyListener{
		public void keyPressed(KeyEvent e) {
		    int keyCode = e.getKeyCode();
		    switch( keyCode ) { 
		        case KeyEvent.VK_UP:
		        	jugar("Arriba");
					sonido("Ficha");
					actualizarFichas(); 
		            break;
		        case KeyEvent.VK_DOWN:
		        	jugar("Abajo");
					sonido("Ficha");
					actualizarFichas();
		            break;
		        case KeyEvent.VK_LEFT:
		        	jugar("Izquierda");
					sonido("Ficha");
					actualizarFichas();
		            break;
		        case KeyEvent.VK_RIGHT :
		        	jugar("Derecha");
					sonido("Ficha");
					actualizarFichas();
		            break;
		     }
		}

		public void keyReleased(KeyEvent e) {}
		public void keyTyped(KeyEvent e) {} 
	}
	
	private void sonido(String sonido) {
		switch(sonido){
			case "Ficha":
				 try {
			        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("Recursos\\pitido.wav").getAbsoluteFile());
			        Clip clip = AudioSystem.getClip();
			        clip.open(audioInputStream);
			        clip.start();
			    } catch(Exception ex) {
			        System.out.println("Error with playing sound.");
			        ex.printStackTrace();
			    }
				break;
			case "Victoria":
				try {
			        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("Recursos\\victoria.wav").getAbsoluteFile());
			        Clip clip = AudioSystem.getClip();
			        clip.open(audioInputStream);
			        clip.start();
			    } catch(Exception ex) {
			        System.out.println("Error with playing sound.");
			        ex.printStackTrace();
			    }
				break;
			case "Game Over":
				try {
			        AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File("Recursos\\game over.wav").getAbsoluteFile());
			        Clip clip = AudioSystem.getClip();
			        clip.open(audioInputStream);
			        clip.start();
			    } catch(Exception ex) {
			        System.out.println("Error with playing sound.");
			        ex.printStackTrace();
			    }		 
		}
	   
	}

	public void nuevaPartida(){
		this.tablero = new Tablero();
		this.actualizarFichas();
	}
	
	private class Volver implements ActionListener{
	    @Override
	    public void actionPerformed(ActionEvent e) {
	        ventana.setVisible(false);
	        menu.setVisible(true);
	    }
	}
}
