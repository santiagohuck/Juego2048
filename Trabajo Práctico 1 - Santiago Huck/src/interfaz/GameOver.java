package interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;

import java.awt.Color;

import javax.swing.SwingConstants;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;


public class GameOver {

	private JFrame ventana;
	private int puntajeFinal;
	private JTextField nombreText;
	private JLabel titulo, nombre, puntaje, puntosFinales;
	private JButton botonVolver, botonRegistrarYVolver;
	private Menu menu;
	private Records records;

	
	public GameOver(Menu menu, Records records) {
		this.menu = menu;
		this.records = records;
		this.titulo = new JLabel("                                                           ");
		
		inicializar();
	}
	
	public void setVisible(boolean visibilidad){
		this.ventana.setVisible(visibilidad);
	}
	
	public void setPuntaje(int puntos){
		this.puntajeFinal = puntos;
		puntosFinales.setText(this.puntajeFinal+"");
	}
	
	private void inicializar() {
		
		iniciarVentana();
		iniciarTitulo();
		iniciarBotones();	
		iniciarJLabels();
		iniciarJTextFields();
		
	}
	
	private void iniciarVentana(){
		
		ventana = new JFrame();
		ventana.setBounds(500, 150, 450, 300);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.getContentPane().setLayout(null);
		
	}
	
	private void iniciarTitulo(){
		
		titulo.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 50));
		titulo.setForeground(new Color(153, 51, 0));
		titulo.setHorizontalAlignment(SwingConstants.CENTER);
		titulo.setBounds(32, 11, 377, 96);
		ventana.getContentPane().add(titulo);
		
	}
	
	private void iniciarBotones(){
		
		botonVolver = new JButton("Volver");
		botonVolver.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 11));
		botonVolver.setBounds(296, 214, 89, 23);
		botonVolver.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonVolver);
		botonVolver.addActionListener(new Volver());
		
		botonRegistrarYVolver = new JButton("Registrar y Volver");
		botonRegistrarYVolver.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 11));
		botonRegistrarYVolver.setBounds(276, 166, 129, 23);
		botonRegistrarYVolver.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(botonRegistrarYVolver);
		botonRegistrarYVolver.addActionListener(new RegistrarVolver());
		
	}
	
	private void iniciarJLabels(){
		
		nombre = new JLabel("Nombre:");
		nombre.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 16));
		nombre.setForeground(new Color(153, 51, 0));
		nombre.setBounds(41, 164, 89, 23);
		ventana.getContentPane().add(nombre);
				
		puntosFinales = new JLabel("");
		puntosFinales.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 16));
		puntosFinales.setHorizontalAlignment(SwingConstants.CENTER);
		puntosFinales.setForeground(new Color(153, 51, 0));
		puntosFinales.setBounds(140, 131, 56, 23);
		ventana.getContentPane().add(puntosFinales);		
		
		puntaje = new JLabel("Puntaje:");
		puntaje.setForeground(new Color(153, 51, 0));
		puntaje.setFont(new Font("Copperplate Gothic Bold", Font.BOLD, 16));
		puntaje.setBounds(41, 131, 89, 23);
		ventana.getContentPane().add(puntaje);
		
	}
	
	private void iniciarJTextFields(){
		
		nombreText = new JTextField();
		nombreText.setFont(new Font("Copperplate Gothic Bold", Font.PLAIN, 11));
		nombreText.setBounds(134, 168, 86, 20);
		nombreText.setBorder(new LineBorder(new Color(0, 0, 0), 1));
		ventana.getContentPane().add(nombreText);
		nombreText.setColumns(10);
		
	}
	
	public void setResultado(String s){
		this.titulo.setText(s);
	}
	private class Volver implements ActionListener{
	    @Override
	    public void actionPerformed(ActionEvent e) {
	        ventana.setVisible(false);
	        menu.setVisible(true);
	    }
	}
	
	private class RegistrarVolver implements ActionListener{
	    @Override
	    public void actionPerformed(ActionEvent e) {
	    	records.registrarRecord(nombreText.getText(), puntajeFinal); 	
	    	nombreText.setText("");
	    	records.actualizarTabla();
	        ventana.setVisible(false);
	        menu.setVisible(true);
	    }
	}
}
